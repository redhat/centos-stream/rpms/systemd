From 82d97eb8fa19a6753dafcb2b623cb8277213804f Mon Sep 17 00:00:00 2001
From: Thomas Hebb <tommyhebb@gmail.com>
Date: Wed, 18 Dec 2024 11:08:17 -0500
Subject: [PATCH] ukify: Fix regression in --no-sign-kernel flag

The man page says that --sign-kernel and --no-sign-kernel "override the
detection of whether to sign the Linux binary", so we should only
autodetect if neither are specified. But as of commit 02eabaffe98c
("ukify: Add a unified interface for signing tools"), we autodetect even
when --no-sign-kernel is passed, which makes the flag useless.

The sign_kernel option is parsed using argparse.BooleanOptionalAction,
which sets it to either True, False, or None. commit 02eabaffe98c
replaced `sign_kernel is None` with `not sign_kernel`. These are not the
same in Python, as the latter accepts False as well as None.

Restore the original check and fix type annotations accordingly.

Fixes: 02eabaffe98c ("ukify: Add a unified interface for signing tools")
(cherry picked from commit 32c3e1379dce563a7e686c99045549ac74cce142)
---
 src/ukify/ukify.py | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/src/ukify/ukify.py b/src/ukify/ukify.py
index e661dfe548..3f36aa7af6 100755
--- a/src/ukify/ukify.py
+++ b/src/ukify/ukify.py
@@ -264,7 +264,7 @@ class UkifyConfig:
     sbat: Optional[list[str]]
     sections: list['Section']
     sections_by_name: dict[str, 'Section']
-    sign_kernel: bool
+    sign_kernel: Optional[bool]
     signing_engine: Optional[str]
     signing_provider: Optional[str]
     certificate_provider: Optional[str]
@@ -1108,7 +1108,7 @@ def make_uki(opts: UkifyConfig) -> None:
         assert opts.signtool is not None
         signtool = SignTool.from_string(opts.signtool)
 
-        if not sign_kernel:
+        if sign_kernel is None:
             # figure out if we should sign the kernel
             sign_kernel = signtool.verify(opts)
 
