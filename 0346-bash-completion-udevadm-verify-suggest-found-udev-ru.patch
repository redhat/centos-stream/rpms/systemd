From 6aa9e8d394dc04f3432a74e837cd21a3e860a43c Mon Sep 17 00:00:00 2001
From: Yu Watanabe <watanabe.yu+github@gmail.com>
Date: Thu, 9 Jan 2025 19:25:49 +0900
Subject: [PATCH] bash-completion/udevadm-verify: suggest found udev rules
 files

This also fixes the issue that no suggestion is provided after a standalone
option is specified.

(cherry picked from commit bbe1ba5e87a74fa0b56d88a7266040e534c527b0)

Resolves: RHEL-75774
---
 shell-completion/bash/udevadm | 50 ++++++++++++++++++++++++++++-------
 1 file changed, 40 insertions(+), 10 deletions(-)

diff --git a/shell-completion/bash/udevadm b/shell-completion/bash/udevadm
index 496eac5fd7..54d024c1b8 100644
--- a/shell-completion/bash/udevadm
+++ b/shell-completion/bash/udevadm
@@ -46,6 +46,35 @@ __get_all_devices() {
     __get_all_device_units
 }
 
+__get_root() {
+    local i
+
+    for ((i=0; i < COMP_CWORD; i++)); do
+        if [[ "${COMP_WORDS[i]}" = --root=* ]]; then
+             echo "${COMP_WORDS[i]#--root=}"
+             break
+        fi
+        if (( i > 0 )) && [[ "${COMP_WORDS[i-1]}" == "--root" ]]; then
+             echo "${COMP_WORDS[i]}"
+             break
+        fi
+    done
+}
+
+__get_udev_rules_files() {
+    local root=$( __get_root )
+
+    ls "$root"/usr/lib/udev/rules.d/*.rules \
+       "$root"/usr/local/lib/udev/rules.d/*.rules \
+       "$root"/run/udev/rules.d/*.rules \
+       "$root"/etc/udev/rules.d/*.rules 2>/dev/null
+}
+
+__get_udev_rules_names() {
+    local -a rules=( $( __get_udev_rules_files ) )
+    printf '%s\n' "${rules[@]##*/}"
+}
+
 _udevadm() {
     local i verb comps builtin
     local cur=${COMP_WORDS[COMP_CWORD]} prev=${COMP_WORDS[COMP_CWORD-1]}
@@ -72,7 +101,8 @@ _udevadm() {
         [TEST_STANDALONE]='-v --verbose'
         [TEST_ARG]='-a --action -N --resolve-names -D --extra-rules-dir'
         [TEST_BUILTIN]='-a --action'
-        [VERIFY]='-N --resolve-names --root --no-summary --no-style'
+        [VERIFY_STANDALONE]='--no-summary --no-style'
+        [VERIFY_ARG]='-N --resolve-names --root'
         [WAIT]='-t --timeout --initialized=no --removed --settle'
         [LOCK]='-t --timeout -d --device -b --backing -p --print'
     )
@@ -266,27 +296,27 @@ _udevadm() {
             ;;
 
         'verify')
-            if __contains_word "$prev" ${OPTS[VERIFY]}; then
+            if __contains_word "$prev" ${OPTS[VERIFY_ARG]}; then
                 case $prev in
                     -N|--resolve-names)
                         comps='early never'
                         ;;
                     --root)
-                        comps=$(compgen -A directory -- "$cur" )
+                        comps=''
                         compopt -o dirnames
                         ;;
                     *)
                         comps=''
                         ;;
                 esac
-                COMPREPLY=( $(compgen -W '$comps' -- "$cur") )
-                return 0
-            fi
-
-            if [[ $cur = -* ]]; then
-                comps="${OPTS[COMMON]} ${OPTS[VERIFY]}"
+            elif [[ $cur = -* ]]; then
+                comps="${OPTS[COMMON]} ${OPTS[VERIFY_ARG]} ${OPTS[VERIFY_STANDALONE]}"
+            elif [[ $cur = */* ]]; then
+                comps=$( __get_udev_rules_files )
+                compopt -o dirnames
             else
-                comps=$( compgen -A file -- "$cur" )
+                comps=$( __get_udev_rules_names )
+                compopt -o default
             fi
             ;;
 
