From e8d5d7f355ae826f4f8c0f61f62c31e828bde7d0 Mon Sep 17 00:00:00 2001
From: Daan De Meyer <daan.j.demeyer@gmail.com>
Date: Tue, 4 Feb 2025 14:52:02 +0100
Subject: [PATCH] stub: Mention that VirtualSize should be <= SizeOfRawData

(cherry picked from commit 2443b4d9a17787fd0a63d6591fbdb74650c43994)
---
 man/systemd-stub.xml | 9 +++++++++
 1 file changed, 9 insertions(+)

diff --git a/man/systemd-stub.xml b/man/systemd-stub.xml
index 902b4013a0..779867f4d6 100644
--- a/man/systemd-stub.xml
+++ b/man/systemd-stub.xml
@@ -376,6 +376,15 @@
     core kernel, the embedded initrd and kernel command line (see above for a full list), including all UKI
     profiles.</para>
 
+    <para>Also note that when <command>systemd-stub</command> measures a PE section, it will measure the
+    amount of bytes that the section takes up in memory (<varname>VirtualSize</varname>) and not the amount
+    of bytes that the section takes up on disk (<varname>SizeOfRawData</varname>). This means that if the
+    size in memory is larger than the size on disk, <command>systemd-stub</command> will end up measuring
+    extra zeroes. To avoid this from happening, it is recommended to make sure that the size in memory of
+    each section that is measured by <command>systemd-stub</command> is always smaller than or equal to the
+    size on disk. <command>ukify</command> automatically makes sure this is the case when building UKIs or
+    addons.</para>
+
     <para>Also note that the Linux kernel will measure all initrds it receives into TPM PCR 9. This means
     every type of initrd (of the selected UKI profile) will possibly be measured two or three times: the
     initrds embedded in the kernel image will be measured to PCR 4, PCR 9 and PCR 11; the initrd synthesized
