From 24cb20f7d53015dace0a2a7621c7671ab54b1e81 Mon Sep 17 00:00:00 2001
From: Mike Yuan <me@yhndnzj.com>
Date: Sat, 11 Jan 2025 16:26:55 +0100
Subject: [PATCH] bus-util: do not reset the count returned by
 sd_bus_track_count_name()

Follow-up for 8402ca04d1a063c3d8a9e3d5c16df8bb8778ae98

While at it, turn the retval check for sd_bus_track_count_name()
into assertion, given we're working with already established tracks
(service_name_is_valid() should never yield false in this case).

Addresses https://github.com/systemd/systemd/pull/35406#discussion_r1912066774

(cherry picked from commit 33eeea4128f31df7ab4bd8866b582062d70114ae)

Resolves: RHEL-73780
---
 src/shared/bus-util.c | 11 +++++------
 1 file changed, 5 insertions(+), 6 deletions(-)

diff --git a/src/shared/bus-util.c b/src/shared/bus-util.c
index bdce97790d..1fda013697 100644
--- a/src/shared/bus-util.c
+++ b/src/shared/bus-util.c
@@ -700,16 +700,15 @@ int bus_track_add_name_many(sd_bus_track *t, char **l) {
 
 int bus_track_to_strv(sd_bus_track *t, char ***ret) {
         _cleanup_strv_free_ char **subscribed = NULL;
-        int r = 0;
+        int r;
 
         assert(ret);
 
         for (const char *n = sd_bus_track_first(t); n; n = sd_bus_track_next(t)) {
-                r = sd_bus_track_count_name(t, n);
-                if (r < 0)
-                        return r;
+                int c = sd_bus_track_count_name(t, n);
+                assert(c >= 0);
 
-                for (int j = 0; j < r; j++) {
+                for (int j = 0; j < c; j++) {
                         r = strv_extend(&subscribed, n);
                         if (r < 0)
                                 return r;
@@ -717,7 +716,7 @@ int bus_track_to_strv(sd_bus_track *t, char ***ret) {
         }
 
         *ret = TAKE_PTR(subscribed);
-        return r;
+        return 0;
 }
 
 int bus_open_system_watch_bind_with_description(sd_bus **ret, const char *description) {
